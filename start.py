import luigi
from Classes.CleanData import CleanData
import pandas as pd
from Classes.Utils import create_directory
from Classes.Utils import get_logger
import subprocess
from Classes.Supervised.LinearRegression import LinearRegression
from Classes.Supervised.RandomForestClassifier_ import RandomForestClassifier_
from Classes.Supervised.RandomForestRegressor_ import RandomForestRegressor_
from Classes.Supervised.kNNClassifier import kNNClassifier
from Classes.Supervised.LogisticRegression import LogisticRegression
from Classes.Supervised.NeuralNetworkClassifier_ import NeuralNetworkClassifier_
from Classes.Supervised.NeuralNetworkRegressor_ import NeuralNetworkRegressor_
from Classes.Supervised.SVM_ import SVM_



import numpy as np

class Start(luigi.Task):

 
    def requires(self):
        return [CleanData()]
 
    def output(self):
        return luigi.LocalTarget("Data/Results/results.csv")
 
    def run(self):
        # Adding logger
        logger = get_logger()
        logger.info("Test Log")


        # ___________________________

        create_directory("Results")
        cleaned_dir = "Data/Cleaned/"
        home_dir = ""
        # READ DATA INTO DATAFRAME
        irisData = pd.read_csv(cleaned_dir + 'cleaned_irisdataset.csv')

        # ____________________________________________________________________
        # DESCRIBE DATASET --JUST TO TEST 
        print(irisData.describe())
        grouped_data = irisData.groupby(['iris_class'])
        print(grouped_data.describe().unstack())
        # ____________________________________________________________________


        # SPLIT DATASET INTO TRAIN AND TEST

        msk = np.random.rand(len(irisData)) < 0.8

        trainData = irisData[msk]

        testData = irisData[~msk]

        target = "iris_class"

        predictorVariables = ['sepal_length_in_cm', 'sepal_width_in_cm', 'petal_length_in_cm', 'petal_width_in_cm']
        
        X_train = trainData[predictorVariables]
        X_test = testData[predictorVariables]
        y_train = trainData[target]
        y_test = testData[target]
        

        # RUN PREDICTION OR CLASSIFICATION ALGORITHM ...... 
        LinearRegression(X_train,X_test, y_train, y_test)


        RandomForestClassifier_(X_train,X_test, y_train, y_test)


        kNNClassifier(X_train,X_test, y_train, y_test)


        LogisticRegression(X_train,X_test, y_train, y_test)


        SVM_(X_train,X_test, y_train, y_test)


        RandomForestRegressor_(X_train,X_test, y_train, y_test)


        NeuralNetworkClassifier_(X_train,X_test, y_train, y_test)


        NeuralNetworkRegressor_(X_train,X_test, y_train, y_test)

        # print(irisData)

        # ___________________________________-



        # Removing Logger
        logger.removeHandler("handler")
        logging.shutdown()


if __name__ == '__main__':
    luigi.run()