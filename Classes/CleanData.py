import luigi
from Classes.GetData import GetData
import pandas as pd
from Classes.Utils import create_directory


def create_dummy_variable(irisData):
    # iris_class_df = pd.get_dummies(irisData['iris_class'])
    # irisData = irisData.join(iris_class_df)
    irisData.loc[(irisData['iris_class'] == 'Iris-setosa'), ['iris_class']] = 1
    irisData.loc[(irisData['iris_class'] == 'Iris-versicolor'), ['iris_class']] = 2
    irisData.loc[(irisData['iris_class'] == 'Iris-virginica'), ['iris_class']] = 3


    return irisData

class CleanData(luigi.Task):
    def requires(self):
        return [GetData()]
 
    def output(self):
        return luigi.LocalTarget("Data/Cleaned/cleaned_irisdataset.csv")
 
    def run(self):
        create_directory("Data/Cleaned")
        downloads_dir = "Data/Downloads/"
        cleaned_dir = "Data/Cleaned/"

        # READ DATA INTO DATAFRAME
        irisData = pd.read_csv(downloads_dir + 'irisdataset.data', sep = ",", header = None,
                             names = ['sepal_length_in_cm', 'sepal_width_in_cm', 'petal_length_in_cm', 'petal_width_in_cm', 'iris_class'])


        # CLEAN DATA
        irisData = create_dummy_variable(irisData)


        # SAVE DATAFRAME
        irisData.to_csv(cleaned_dir + "cleaned_irisdataset.csv", sep=',', index = False)