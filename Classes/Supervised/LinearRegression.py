import numpy as np
import sklearn

from sklearn import linear_model
from sklearn.metrics import *

def LinearRegression(X_train,X_test, y_train, y_test):
	print("______________________START OF LINEAR REGRESSION____________________")

	lm = linear_model.LinearRegression()
	lm.fit(X_train, y_train)


	train_pred = lm.predict(X_train)

	# R-squared score of this model
	
	rsq = r2_score(y_train, train_pred)

	pred = lm.predict(X_test)

	# MEAN ABSOLUTE ERROR
	mean_ae = mean_absolute_error(y_test, pred)

	# MEAN SQUARED ERROR
	mean_sqe = mean_squared_error(y_test, pred)

	# MEDIAN ABSOLUTE ERROR
	median_ae = median_absolute_error(y_test, pred)

	print("R-squared error : " + str(rsq) )

	print("MEAN ABSOLUTE ERROR  : " + str(mean_ae) )

	print("MEAN SQUARED ERROR : " + str(mean_sqe) )

	print("MEDIAN ABSOLUTE ERROR : " + str(median_ae) )

	# Coefficient and Intercept
	coefi = lm.coef_

	intercept = lm.intercept_

	print("Coefficients are : " + str(coefi) + " intercept is : " + str(intercept))


	    #     RMSE
    rmse = mean_sqe**0.5
    
#     rmse2 = (np.sqrt(np.mean((y_test-pred)**2)))
    
    print("RMSE : " + str(rmse))


	# # __________ROC_____________
	# from sklearn.metrics import roc_curve

	# fpr, tpr, _ = roc_curve(y_test, pred)

	# import matplotlib.pyplot as plt
	# plt.figure()
	# plt.plot(fpr, tpr, label = 'ROC curve')
	# plt.plot([0,1], [0,1], 'k--')
	# plt.xlim([0.0, 1.0])
	# plt.ylim([0.0, 1.05])
	# plt.xlabel('1-Specificity')
	# plt.ylabel('Sensitivity')
	# plt.title('ROC Curve')
	# plt.legend(loc-"lower right")
	# plt.show()

	


	print("______________________END OF LINEAR REGRESSION____________________")



def LinearRegressionAfterFeatureSelection(X_train,X_test, y_train, y_test):
    print("______________________START OF LINEAR REGRESSION - Feature Selection____________________")

    lm = linear_model.LinearRegression()
    
    rfe = RFE(lm, 10)
    
    rfe = rfe.fit(X_train, y_train)
    
    # summarize the selection of the attributes
    print(rfe.support_)
    print(rfe.ranking_)


    train_pred = rfe.predict(X_train)

    # R-squared score of this model

    rsq = r2_score(y_train, train_pred)

    pred = rfe.predict(X_test)

    # MEAN ABSOLUTE ERROR
    mean_ae = mean_absolute_error(y_test, pred)

    # MEAN SQUARED ERROR
    mean_sqe = mean_squared_error(y_test, pred)

    # MEDIAN ABSOLUTE ERROR
    median_ae = median_absolute_error(y_test, pred)

    print("R-squared error : " + str(rsq) )

    print("MEAN ABSOLUTE ERROR  : " + str(mean_ae) )

    print("MEAN SQUARED ERROR : " + str(mean_sqe) )

    print("MEDIAN ABSOLUTE ERROR : " + str(median_ae) )

    #     RMSE
    rmse = mean_sqe**0.5
    
#     rmse2 = (np.sqrt(np.mean((y_test-pred)**2)))
    
    print("RMSE : " + str(rmse))

    print("______________________END OF LINEAR REGRESSION - Feature Selection____________________")