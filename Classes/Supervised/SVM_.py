import numpy as np
import sklearn
import pandas as pd
from sklearn import svm
from sklearn.metrics import *

def SVM_(X_train,X_test, y_train, y_test):


	print("______________________START OF SVM____________________")

	clf = svm.SVC()
	clf.fit(X_train, y_train)
	
	# result = pd.crosstab(y_test, X_test, rownames=['actual'], colnames=['preds'])

	pred = clf.predict(X_test)
	

	print (accuracy_score(y_test, pred))


	# CONFUSION MATRIX
	confusion_mat = confusion_matrix(y_test, pred, labels=[1, 2, 3])
	print(confusion_mat)

	print (pd.crosstab(y_test, pred, rownames=["Actual"], colnames=["Pred"]))


	print("______________________END OF SVM____________________")