import numpy as np
import sklearn
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import *

def RandomForestClassifier_(X_train,X_test, y_train, y_test):

	print("______________________START OF RANDOM FOREST CLASSIFIER____________________")


	clf = RandomForestClassifier(n_jobs=2)
	clf.fit(X_train, y_train)

	# result = pd.crosstab(y_test, X_test, rownames=['actual'], colnames=['preds'])

	pred = clf.predict(X_test)
	

	print (accuracy_score(y_test, pred))


	# CONFUSION MATRIX
	confusion_mat = confusion_matrix(y_test, pred, labels=[1, 2, 3])
	print(confusion_mat)


	print("______________________END OF RANDOM FOREST CLASSIFIER____________________")