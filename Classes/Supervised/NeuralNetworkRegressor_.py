import numpy as np
from sklearn.neural_network import MLPRegressor

from sklearn.metrics import *

def NeuralNetworkRegressor_(X_train,X_test, y_train, y_test):

	print("______________________START OF NEURAL NETWORK REGRESSOR____________________")

	reg = MLPRegressor(hidden_layer_sizes=(100, ), activation='relu', solver='adam',
					 alpha=0.0001, batch_size='auto', learning_rate='constant', learning_rate_init=0.001,
					  power_t=0.5, max_iter=200, shuffle=True, random_state=None, tol=0.0001, verbose=False,
					   warm_start=False, momentum=0.9, nesterovs_momentum=True, early_stopping=False,
					    validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

	reg.fit(X_train, y_train)


	

	pred = reg.predict(X_test)

	train_pred = reg.predict(X_train)

	# R-squared score of this model
	
	rsq = r2_score(y_train, train_pred)

	# MEAN ABSOLUTE ERROR
	mean_ae = mean_absolute_error(y_test, pred)

	# MEAN SQUARED ERROR
	mean_sqe = mean_squared_error(y_test, pred)

	# MEDIAN ABSOLUTE ERROR
	median_ae = median_absolute_error(y_test, pred)

	print("R-squared error : " + str(rsq) )

	print("MEAN ABSOLUTE ERROR  : " + str(mean_ae) )

	print("MEAN SQUARED ERROR : " + str(mean_sqe) )

	print("MEDIAN ABSOLUTE ERROR : " + str(median_ae) )



	


	print("______________________END OF Neural Network Regressor____________________")



