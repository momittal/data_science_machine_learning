import numpy as np
import sklearn
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import *

def RandomForestRegressor_(X_train,X_test, y_train, y_test):

	print("______________________START OF RANDOM FOREST REGRESSOR____________________")

	regressor = RandomForestRegressor(n_estimators=150, min_samples_split=2)
	regressor.fit(X_train, y_train)

	# result = pd.crosstab(y_test, X_test, rownames=['actual'], colnames=['preds'])

	pred = regressor.predict(X_test)
	

	# print (accuracy_score(y_test, pred))

	# EVALUATION
	# r2 = r2_score(test.alcohol, rf.predict(test[cols]))
	# mse = np.mean((test.alcohol - rf.predict(test[cols]))**2)

	train_pred = regressor.predict(X_train)
	
	rsq = r2_score(y_train, train_pred)

	# MEAN ABSOLUTE ERROR
	mean_ae = mean_absolute_error(y_test, pred)

	# MEAN SQUARED ERROR
	mean_sqe = mean_squared_error(y_test, pred)

	# MEDIAN ABSOLUTE ERROR
	median_ae = median_absolute_error(y_test, pred)

	print("R-squared error : " + str(rsq) )

	print("MEAN ABSOLUTE ERROR  : " + str(mean_ae) )

	print("MEAN SQUARED ERROR : " + str(mean_sqe) )

	print("MEDIAN ABSOLUTE ERROR : " + str(median_ae) )



	print("______________________END OF RANDOM FOREST REGRESSOR____________________")