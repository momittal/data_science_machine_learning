import numpy as np
import sklearn
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import *
from sklearn.metrics import confusion_matrix

def kNNClassifier(X_train,X_test, y_train, y_test):


	print("______________________START OF kNN____________________")

	knn = KNeighborsClassifier()
	knn.fit(X_train, y_train) 
	KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski', metric_params=None, n_jobs=1, n_neighbors=5, p=2, weights='uniform')

	# result = pd.crosstab(y_test, X_test, rownames=['actual'], colnames=['preds'])

	pred = knn.predict(X_test)


	print (accuracy_score(y_test, pred))


	# CONFUSION MATRIX
	confusion_mat = confusion_matrix(y_test, pred)
	print(confusion_mat)

	print("______________________END OF kNN____________________")