import numpy as np
import sklearn

from sklearn import linear_model
from sklearn.metrics import *

def LogisticRegression(X_train,X_test, y_train, y_test):

	print("______________________START OF LOGISTIC REGRESSION____________________")

	lm = linear_model.LogisticRegression()
	lm.fit(X_train, y_train)

	train_pred = lm.predict(X_train)

	# R-squared score of this model
	
	rsq = r2_score(y_train, train_pred)

	pred = lm.predict(X_test)

	# MEAN ABSOLUTE ERROR
	mean_ae = mean_absolute_error(y_test, pred)

	# MEAN SQUARED ERROR
	mean_sqe = mean_squared_error(y_test, pred)

	# MEDIAN ABSOLUTE ERROR
	median_ae = median_absolute_error(y_test, pred)

	print("R-squared error : " + str(rsq) )

	print("MEAN ABSOLUTE ERROR  : " + str(mean_ae) )

	print("MEAN SQUARED ERROR : " + str(mean_sqe) )

	print("MEDIAN ABSOLUTE ERROR : " + str(median_ae) )



	print (accuracy_score(y_test, pred))


	# CONFUSION MATRIX
	confusion_mat = confusion_matrix(y_test, pred)
	print(confusion_mat)





	print("______________________END OF LOGISTIC REGRESSION____________________")



