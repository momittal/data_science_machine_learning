import numpy as np
from sklearn.neural_network import MLPClassifier

from sklearn.metrics import *

def NeuralNetworkClassifier_(X_train,X_test, y_train, y_test):

	print("______________________START OF NEURAL NETWORK CLASSIFIER____________________")
	clf = MLPClassifier(solver = 'lbfgs', alpha=1e-5, hidden_layer_sizes=(5,2), random_state=1)

	clf.fit(X_train, y_train)


	

	pred = clf.predict(X_test)

	train_pred = clf.predict(X_train)

	# R-squared score of this model
	
	rsq = r2_score(y_train, train_pred)

	# MEAN ABSOLUTE ERROR
	mean_ae = mean_absolute_error(y_test, pred)

	# MEAN SQUARED ERROR
	mean_sqe = mean_squared_error(y_test, pred)

	# MEDIAN ABSOLUTE ERROR
	median_ae = median_absolute_error(y_test, pred)

	print("R-squared error : " + str(rsq) )

	print("MEAN ABSOLUTE ERROR  : " + str(mean_ae) )

	print("MEAN SQUARED ERROR : " + str(mean_sqe) )

	print("MEDIAN ABSOLUTE ERROR : " + str(median_ae) )


	print (accuracy_score(y_test, pred))


	# CONFUSION MATRIX
	confusion_mat = confusion_matrix(y_test, pred, labels=[1, 2, 3])
	print(confusion_mat)
	


	print("______________________END OF Neural Network Classifier____________________")



