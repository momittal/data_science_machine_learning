import luigi
from Classes.Utils import create_directory

class GetData(luigi.Task):
    
    def requires(self):
        return []
 
    def output(self):
        return luigi.LocalTarget("Data/Downloads/irisdataset.data")
 
    def run(self):
    	create_directory("Data")
    	create_directory("Data/Downloads")
    	downloads_dir = "Data/Downloads/"
    	# CODE TO DOWNLOAD DATASET INTO THE DIRECTORY
